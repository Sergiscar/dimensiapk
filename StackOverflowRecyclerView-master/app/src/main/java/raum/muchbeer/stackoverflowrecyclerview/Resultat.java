package raum.muchbeer.stackoverflowrecyclerview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

import static android.widget.Toast.LENGTH_SHORT;

public class Resultat extends AppCompatActivity {

    TextView installation;
    TextView zone;
    TextView nombrepanneauparal;
    TextView nombrepanneauserie;
    TextView nombrebatterieparal;
    TextView nombrebatterieser;
    TextView nompanneau;
    TextView nombatterie;
    TextView besoinjr;
    TextView tensionsysteme;
    TextView nombrepanneau;
    TextView iccs;
    TextView nb;
    TextView pc;
    TextView capacite;
    Button btn;
    String text="a";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultat);
        zone = (TextView)findViewById(R.id.viewzone);
        nombrepanneauparal = (TextView)findViewById(R.id.viewpanneauparrallele);
        nombrepanneauserie = (TextView)findViewById(R.id.viewpanneauserie);
        nombrebatterieparal = (TextView)findViewById(R.id.view_nombre_batterie_parallele) ;
        nombrebatterieser = (TextView)findViewById(R.id.view_nombre_batterie_serie);
        nombatterie = (TextView)findViewById(R.id.view_nom_batterie);
        nompanneau = (TextView)findViewById(R.id.nompanneau);
        installation = (TextView)findViewById(R.id.viewinstallation);
        besoinjr = (TextView)findViewById(R.id.viewbesoinjournalier);
        tensionsysteme = (TextView)findViewById(R.id.viewtensionsysteme);
        nombrepanneau = (TextView)findViewById(R.id.viewnombrepanneau);
        nombatterie = (TextView)findViewById(R.id.view_nom_batterie);

        iccs = (TextView)findViewById(R.id.viewiccs);
        nb = (TextView)findViewById(R.id.view_nombre_batterie);
        pc = (TextView)findViewById(R.id.viewpc);
        capacite = (TextView)findViewById(R.id.viewcapacitesysteme);

        if(Stockage.typestockage.matches("sanstockage")) {

            final  String insta="Type d'installation : Sans stockage";
            final  String myzone="Commune : "+ Zone.maville;
            final String tension="Tension du système : " + Adapter.tensionsysteme+ "V";
            final String besoin ="Bésoin journalier est de : "+ Infosansstockage.besoinjr+"Kw/h";
            final String nompanneauu ="Panneau : "+ Infosansstockage.nomPanneau;
            final String nombrepanneauparallele ="Pannneau en parrallele : "+ Infosansstockage.nombrepanneauparrallele;
            final String nombrepanneauenseri ="Pannneau en serie : "+ Infosansstockage.nombrepanneauserie;
            final String panneau ="Nombre total de panneau : "+ Infosansstockage.nombrepanneau+"";
            final String icc= "Le régulateur choisi doit avoir une intensité supérieur à "+new DecimalFormat("#.##").format(Infosansstockage.iccs) +"A";
            final String pcc= "Puissance crète du système : "+new DecimalFormat("#.##").format(Adapter.pc)+"Wc";
            installation.setText(insta);
            besoinjr.setText(besoin);
            nombatterie.setText("Pas de batterie");
            nombatterie.setTextColor(Color.parseColor("#FF0000"));
            tensionsysteme.setText(tension);
            nombrepanneau.setText(panneau);
            zone.setText(myzone);
            iccs.setText(icc);
            pc.setText(pcc);
            nombrepanneauparal.setText(nombrepanneauparallele);
            nompanneau.setText(nompanneauu);
            text = ""+ insta+"\n"+tension + "\n"+ besoin + "\n"+ panneau+ "\n" +icc + "\n" + pcc;
            nombrepanneauserie.setText(nombrepanneauenseri);
                }
        else{
            Log.d("Nom:  ", Adapter.iccsd + "");
            Log.d("Panneau en serie :", Infostockage.nombrepanneauserie + "");
            final String nompanneaue ="Panneau : "+ Infostockage.nompanneau;
            final String nombrepanneauparallele ="Pannneau en parrallele : "+ Infostockage.nombrepanneauparrallele;
            final String nombrepanneauenseri ="Pannneau en serie : "+ Infostockage.nombrepanneauserie;
            final String panneau ="Nombre de panneau : "+ Infostockage.nombrepanneau;
            final String nombrebatterieparalleles ="Batterie en parralleles : "+ Infostockage.nombrebatterieparrallele;
            final String nombrebatterieenseries ="Batterie en series : "+ Infostockage.nombrebatterieserie;
            final String nombatteries ="Batterie: "+ Infostockage.nombatterie;
            final  String insta="Type d'installation : Avec stockage";
            final String tension="Tension du système : " + Adapter.tensionsysteme+ "V";
            final String besoin ="Bésoin journalier: "+ Adapter.besoinjr+"Kw/h";
            final String icc= "Le régulateur choisi doit avoir une intensité supérieur à "+new DecimalFormat("#.#").format(Infostockage.iccs) +"A";
            final String pcc= "Puissance crête du système: "+new DecimalFormat("#.##").format(Adapter.pc)+"Wc";
            final String batterie= "Nombre de batterie: "+Infostockage.nb+" batteries";
            final String capacity= "La capacité du systeme est "+new DecimalFormat("#.#").format(Infostockage.capacitesysteme)+"Ah";
            final  String myzone="Commune : "+ Zone.maville;

            nombrepanneauparal.setText(nombrepanneauparallele);
            nompanneau.setText(nompanneaue);
            nombrepanneauserie.setText(nombrepanneauenseri);
            nombrebatterieparal.setText(nombrebatterieparalleles);
            nombrebatterieser.setText(nombrebatterieenseries);
            nombatterie.setText(nombatteries);
            nompanneau.setText(nompanneaue);
            nombrepanneauserie.setText(nombrepanneauenseri);
            installation.setText(insta);
            zone.setText(myzone);
            besoinjr.setText(besoin);
            tensionsysteme.setText(tension);
            nombrepanneau.setText(panneau);
            iccs.setText(icc);
            pc.setText(pcc);
            capacite.setText(capacity);
            nb.setText(batterie);
            text = ""+ insta+"\n"+tension + "\n"+ besoin + "\n"+ panneau+ "\n" +icc + "\n" + pcc + "\n" + batterie +"\n" + capacity;
            }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.consult, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id==R.id.appeler){

            String dial = "tel:67770605";
            startActivity(new Intent (Intent.ACTION_DIAL, Uri.parse(dial)));
        }
        else if(id==R.id.mail){

            Log.i("Send email", "");
            Log.d("Energie totale:", text + "");
            String[] TO = {"dsgiscar@gmail.com"};
            String[] CC = {"eze.ferret@gmail.com"};
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto"));
            emailIntent.setType("text/plain");

            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
            emailIntent.putExtra(Intent.EXTRA_CC, CC);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Demande de dévis");
            emailIntent.putExtra(Intent.EXTRA_TEXT, text);
            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail"));
                finish();

            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(Resultat.this, " no mail client", Toast.LENGTH_SHORT).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }


}
