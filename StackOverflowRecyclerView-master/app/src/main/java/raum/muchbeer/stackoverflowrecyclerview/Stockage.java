package raum.muchbeer.stackoverflowrecyclerview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Stockage extends AppCompatActivity {
    public static double type;
    Button btnAvecStockage;
    Button btnSanStockage;
    public static String typestockage;

    static{
        typestockage="";
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stockage);
        btnAvecStockage = (Button) findViewById(R.id.stockage);
        btnAvecStockage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typestockage="avecstockage";
                Intent intent = new Intent(getApplicationContext(),  Zone.class);
                startActivity(intent);

            }
        });
        btnSanStockage = (Button) findViewById(R.id.sansstockage);
        btnSanStockage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typestockage="sanstockage";
                Intent intent = new Intent(getApplicationContext(), Zone.class);
                startActivity(intent);

            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.propos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), Propos.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
