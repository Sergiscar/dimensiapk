package raum.muchbeer.stackoverflowrecyclerview;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.*;

public class Adapter extends RecyclerView.Adapter<Adapter.TitleAdapter> {
    public static double besoinjr;
    public static double tensionsysteme ;
    public static double capacitesysteme ;
    public static int nombrepanneau;
    public static double iccs;
    public static double iccsd;
    public static int nombrebatterieparrallele;
    public static int nombrebatterieserie;
    public static int nombrepanneauparrallele;
    public static int nombrepanneauserie;
    public static String nomPanneau;
    public static String nomBatterie;
    public static int nb;
    public static double pc;
    private static final String LOG_TAG = Adapter.class.getSimpleName() ;
    private ArrayList<RecyclerData> myList;
    int mLastPosition = 0;
    private RemoveClickListner mListner;
    private static HashMap<Integer, Integer> mesvar =new HashMap<Integer, Integer>();
    static {
        besoinjr=0;
        tensionsysteme=0;
        capacitesysteme=0;
        nombrepanneau=0;
        iccs=0;
        nb=0;
        pc=0;
        nombrebatterieparrallele=0;
        nombrebatterieserie=0;
        nombrepanneauparrallele=0;
        nombrepanneauserie=0;
        mesvar =new HashMap<Integer, Integer>();
    }

    public Adapter(ArrayList<RecyclerData> myList, RemoveClickListner mListner) {
        this.myList = myList;
        this.mListner = mListner;
    }

    @NonNull
    @Override
    public TitleAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item,
                parent,
                false);
        TitleAdapter viewHolder = new TitleAdapter(view);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull TitleAdapter holder, int position)
    {
        Log.d("His problem here is:  ", myList.size() + "");
        int energie = 0;
        int puissancetotale = 0;

        for (int counter = 0; counter < myList.size(); counter++) {


            RecyclerData titleData = myList.get(position);

            mLastPosition = position;
            if (titleData.title == "Lampe") {
                holder.crossImage.setImageResource(R.drawable.lampe);
            } else if (titleData.title == "Télévision") {
                holder.crossImage.setImageResource(R.drawable.tele);

            } else if (titleData.title == "Ventilateur") {
                holder.crossImage.setImageResource(R.drawable.ventilo);
            } else if (titleData.title == "Climatiseur") {
                holder.crossImage.setImageResource(R.drawable.climatiseur);
            } else if (titleData.title == "Fer à repasser") {
                holder.crossImage.setImageResource(R.drawable.fer);
            } else if (titleData.title == "Radio") {
                holder.crossImage.setImageResource(R.drawable.radio);
            } else if (titleData.title == "Brasseur") {
                holder.crossImage.setImageResource(R.drawable.brasseur);
            } else if (titleData.title == "Lampe de Chevet") {
                holder.crossImage.setImageResource(R.drawable.lampechevet);
            } else if (titleData.title == "Veilleuse") {
                holder.crossImage.setImageResource(R.drawable.veilleuse);
            } else if (titleData.title == "Imprimante") {
                holder.crossImage.setImageResource(R.drawable.imprimante);
            } else if (titleData.title == "Décodeur") {
                holder.crossImage.setImageResource(R.drawable.decodeur);
            } else if (titleData.title == "Micro-onde") {
                holder.crossImage.setImageResource(R.drawable.microonde);
            } else if (titleData.title == "DVD") {
                holder.crossImage.setImageResource(R.drawable.dvd);
            } else {
                holder.crossImage.setImageResource(R.drawable.autre);
            }


            energie = Integer.parseInt(titleData.puissance) * Integer.parseInt(titleData.duree) * Integer.parseInt(titleData.nombre);
            puissancetotale = Integer.parseInt(titleData.puissance) * Integer.parseInt(titleData.nombre);
            titleData.energie = energie;


           /* int keytobechecked = position;

            Iterator<Map.Entry<Integer, Integer>> iterator = mesvar.entrySet().iterator();
            boolean isKeyPresent = false;

            while (iterator.hasNext())
            {
                Map.Entry<Integer, Integer> entry = iterator.next();
                if(keytobechecked == entry.getKey())
                {
                    isKeyPresent = true;
                }

            }*/

            ArrayList<Integer> valuelist = new ArrayList<Integer>(mesvar.keySet());
            System.out.println(valuelist);
            Log.d("mavar", valuelist.size()+ "");
             if (valuelist.size() ==0)
             {
                 mesvar.put(myList.size(), energie);

                 besoinjr = besoinjr + energie;

             }
             else{

                 for (HashMap.Entry<Integer, Integer> mesva : mesvar.entrySet())
                 {
                     Log.d("geti", mesva.getKey()+"");
                     Log.d("position", myList.size()+"");
                     if(mesva.getKey()  == myList.size())
                     {


                         Log.d("Message", "J'existe");
                         Log.d("Vie", besoinjr+"");
                     }
                     else
                     {
                         Log.d("Message", "gogo" + besoinjr);
                         mesvar.put(myList.size(), energie);

                         besoinjr = besoinjr + energie;


                     }

                 }

             }


            Log.d("Hasmap", mesvar+"");




            holder.etTitleTextView.setText("Composant: "+titleData.title);
            holder.etPuissanceTextView.setText("Puissance: "+titleData.puissance + " W");
            holder.etNombreTextView.setText("Nombre: "+titleData.nombre);
            holder.etDureeTextView.setText("Duree: "+titleData.duree +" h/j");
            holder.etTextViewEnergie.setText("Energie: "+energie+" Wh");
            holder.etTextViewPuissance.setText("Puissance jr: "+puissancetotale+" W");
            holder.etPositionTextView.setText("N°: " + (position +1));
            holder.etTitleTextView.requestFocus();
        }
        double irradiation = MainActivity.irradiation;

        pc = besoinjr / (irradiation * 0.8 * 0.8);

        double icc = Infostockage.icc;


        if (pc <= 500) {
            tensionsysteme = 12;
        } else if (pc > 501 && pc <= 2000) {
            tensionsysteme = 24;
        } else if (pc > 2001) {
            tensionsysteme = 48;
        }


    }

    public void notifyData(ArrayList<RecyclerData> myList) {

        this.myList = myList;
        Log.d("notifyData ", myList.size() + "");
        notifyDataSetChanged();
    }

    public void delete(int position) { //removes the row
        myList.remove(position);
        notifyItemRemoved(position);
    }



    @Override
    public int getItemCount() {

        return(null != myList?myList.size():0);
    }

    public class TitleAdapter extends RecyclerView.ViewHolder {

        private final TextView etTitleTextView, etPositionTextView,  etPuissanceTextView, etNombreTextView, etDureeTextView, etTextViewEnergie,etTextViewPuissance;
        private LinearLayout mainLayout;
        public ImageView crossImage;
        public TitleAdapter(@NonNull View itemView) {
            super(itemView);

            etTitleTextView = itemView.findViewById(R.id.txtTitle);
            etPositionTextView = itemView.findViewById(R.id.position);
            crossImage = (ImageView) itemView.findViewById(R.id.thumbnail);
            mainLayout = (LinearLayout) itemView.findViewById(R.id.mainLayout);
            etPuissanceTextView = itemView.findViewById(R.id.puissance);
            etNombreTextView = itemView.findViewById(R.id.nombre);
            etDureeTextView = itemView.findViewById(R.id.duree);
            etTextViewEnergie = itemView.findViewById(R.id.energie);
            etTextViewPuissance = itemView.findViewById(R.id.puissanceequip);



            mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext(), "Position:" + getAdapterPosition(), Toast.LENGTH_SHORT).show();
                }
            });

            crossImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    delete(getAdapterPosition());
                    // private ArrayList<RecyclerData> myList;
                    myList.remove(getPosition());
                }
            });
        }
    }
}
