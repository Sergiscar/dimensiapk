package raum.muchbeer.stackoverflowrecyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static java.sql.Types.NULL;

public class MainActivity extends AppCompatActivity implements RemoveClickListner{

    private RecyclerView mRecyclerView;
    private Adapter mRecyclerAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static double irradiation;


    Button btnAddItem;
    Button btnNext;
    ArrayList<RecyclerData> myList = new ArrayList<>();
    EditText etTitle;
    EditText editPuissance;
    EditText editNombre;
    EditText editDuree;
    EditText composant;
    String title ="";
    String nombre = "";
    String duree = "";
    String puissance="";
    Spinner spinner;
    ImageView crossImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle extras = getIntent().getExtras();

        if(extras !=null)
        {
            double Value = extras.getDouble("irradiation");
            if(Value>0) {
                irradiation = Value;
            }
        }
        Log.d("irradiation:", irradiation + "");

        final Spinner dropdown = findViewById(R.id.spinner1);
        String[] years = {"Lampe","Télévision","Ventilateur","Climatiseur","Fer à repasser","Radio","Brasseur","Lampe de Chevet", "DVD", "Pack Wifi",
                "Veilleuse", "Imprimante", "Décodeur", "Lampe-douche", "Micro-onde", "DVD"};
        ArrayAdapter<CharSequence> langAdapter = new ArrayAdapter<CharSequence>(this, R.layout.spinner_text, years );
        langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        dropdown.setAdapter(langAdapter);
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerAdapter = new Adapter(myList,this);


        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mRecyclerAdapter);

        editDuree = (EditText) findViewById(R.id.etitDuree);
        editPuissance = (EditText) findViewById(R.id.editPuissance);
        editNombre = (EditText) findViewById(R.id.editNombre);
        composant = (EditText) findViewById(R.id.editComposant);

              /* if (mRecyclerAdapter.getItemCount() == 0 || mRecyclerAdapter.getItemCount() == NULL){
            Toast.makeText(getApplicationContext(),"Bienvenue",Toast.LENGTH_SHORT).show();
        }*/


        btnAddItem = (Button) findViewById(R.id.btnSave);
        btnNext = (Button) findViewById(R.id.btnnext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mRecyclerAdapter.getItemCount() == 0 || mRecyclerAdapter.getItemCount() == NULL){
                    Toast.makeText(getApplicationContext(),"Vous n'avez entrez aucune valeur pour le moment",Toast.LENGTH_SHORT).show();
                }
                else
                    if(mRecyclerAdapter.getItemCount() != 0 && mRecyclerAdapter.getItemCount() != NULL && Stockage.typestockage.matches("avecstockage")) {
                        Intent intent = new Intent(getApplicationContext(), Infostockage.class);
                        startActivity(intent);
                    }
                     else if(mRecyclerAdapter.getItemCount() != 0 && mRecyclerAdapter.getItemCount() != NULL && Stockage.typestockage.matches("sanstockage")) {
                        Intent intent = new Intent(getApplicationContext(), Infosansstockage.class);
                        startActivity(intent);
                    }




                //   Toast.makeText(view.getContext(), "Page suivante non disponible pour le moment", Toast.LENGTH_SHORT).show();
               // return;

            }
        });
        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String  compo = composant.getText().toString();

                if(compo.matches(""))
                {
                    title = dropdown.getSelectedItem().toString();

                }else
                {
                    title = compo;

                }
                duree = editDuree.getText().toString();
                int dureeint = 0;
                if (!duree.matches("")) {
                    dureeint = Integer.parseInt(duree);
                }


                nombre = editNombre.getText().toString();

                puissance = editPuissance.getText().toString();
                if(duree.matches(""))
                {
                    Toast.makeText(view.getContext(), "Vous n'avez pas entrer la duree", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!duree.matches(""))
                {
                    if(nombre.matches(""))
                    {
                        Toast.makeText(view.getContext(), "Vous n'avez pas entrer le nombre", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    else if(puissance.matches(""))
                    {
                        Toast.makeText(view.getContext(), "Vous n'avez pas entrer la puissance", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else if(nombre.matches("0"))
                    {
                        Toast.makeText(view.getContext(), "Vous avez entrez un mombre invalide", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else if(puissance.matches("0"))
                    {
                        Toast.makeText(view.getContext(), "Vous avez entrez une puissance invalide", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else if(  dureeint >24 || duree.matches("0"))
                    {
                        Toast.makeText(view.getContext(), "Vous avez entrez une duree invalide", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    else
                    {
                        RecyclerData mLog = new RecyclerData();
                        mLog.setTitle(title);
                        mLog.setDuree(duree);
                        mLog.setNombre(nombre);
                        mLog.setPuissance(puissance);
                        myList.add(mLog);
                        mRecyclerAdapter.notifyData(myList);
                        // etTitle.setText("");
                        editPuissance.setText("");
                        editNombre.setText("");
                        editDuree.setText("");
                        composant.setText("");

                    }


                }
            }
        });
    }

    @Override
    public void OnRemoveClick(int index) {
        myList.remove(index);
        mRecyclerAdapter.notifyData(myList);
    }
    public Double getIrradiation() { return irradiation; }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.propos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), Propos.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
