package raum.muchbeer.stackoverflowrecyclerview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Infosansstockage extends AppCompatActivity {
    Button btn;
    Spinner myspin;
    String tensionp ="";
    String mavie="";
    public static double puissancePanneau;
    public static String nomPanneau;
    public static String nomBatterie;
    public static double tensionPanneau;
    public static double tensionBatterie;
    public static double capaciteBatterie;
    public static double icc;
    TextView mypc;
    TextView mytension;

    public static double besoinjr;
    public static String nompanneau;
    public static String nombatterie;
    public static int nombrepanneau;
    public static double iccs;
    public static double tensionsysteme ;
    public static double capacitesysteme ;
    public static double iccsd;
    public static int nombrebatterieparrallele;
    public static int nombrebatterieserie;
    public static int nombrepanneauparrallele;
    public static int nombrepanneauserie;
    public static int nb;

    EditText Tensionpanneau;


    static{
        puissancePanneau=0;
        tensionPanneau=0;
        icc= 0;
        nomPanneau="";
        nomBatterie="";
        tensionPanneau=0;
        besoinjr=0;
        tensionsysteme=0;
        nombrepanneau=0;
        iccs=0;
        nombrepanneauparrallele=0;
        nombrepanneauserie=0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infosansstockage);
        mypc = (TextView)findViewById(R.id.mypc);
        mytension = (TextView)findViewById(R.id.mytension) ;
        final String tension="Tension du système : " + Adapter.tensionsysteme+ "V";
        final String pcc= "Puissance crète du système : "+new DecimalFormat("#.##").format(Adapter.pc)+"Wc";
        mypc.setText(pcc);
        mytension.setText(tension);

        myspin = (Spinner) findViewById(R.id.editPuissancepanneau);

        String[] puissance = {"20W-12V Poly",
                "30W-12V Poly",
                "45W-12V Poly",
                "60W-12V Poly",
                "90W-12V Poly",
                "115W-12V Poly",
                "175W-12V Poly",
                "260W-20V Poly",
                "270W-20V Poly",
                "330W-24V Poly",
                "20W-12W Mono",
                "30W-12V Mono",
                "40W-12V Mono",
                "55W-12V Mono",
                "90W-12V Mono",
                "115W-12V Mono",
                "175W-12V Mono",
                "215W-24V Mono",
                "305W-24V Mono",
                "360W-24V Mono",
        };
        final ArrayList<Panneau> panneaus = new ArrayList<>();
        panneaus.add(new Panneau("20W-12V Poly", 20, 12,1.18));
        panneaus.add(new Panneau("30W-12V Poly", 30, 12,1.8));
        panneaus.add(new Panneau("45W-12V Poly", 45, 12,2.55));
        panneaus.add(new Panneau("60W-12V Poly", 45, 12,3.37));
        panneaus.add(new Panneau("90W-12V Poly", 90, 12,4.98));
        panneaus.add(new Panneau("175W-12V Poly", 175, 12,10.24));
        panneaus.add(new Panneau("115W-12V Poly", 115, 12,6.56));
        panneaus.add(new Panneau("260W-20V Poly", 260, 20,9.3));
        panneaus.add(new Panneau("330W-20V Poly", 260, 20,9.57));
        panneaus.add(new Panneau("270W-20V Poly", 270, 20,9.21));
        panneaus.add(new Panneau("20W-12V Mono", 20, 12,1.19));
        panneaus.add(new Panneau("30W-12V Mono", 30, 12,1.76));
        panneaus.add(new Panneau("40W-12V Mono", 40, 12,2.4));
        panneaus.add(new Panneau("55W-12V Mono", 40, 12,3.22));
        panneaus.add(new Panneau("90W-12V Mono", 40, 12,5.03));
        panneaus.add(new Panneau("115W-12V Mono", 40, 12,6.61));
        panneaus.add(new Panneau("175W-12V Mono", 40, 12,9.89));
        panneaus.add(new Panneau("215W-12V Mono", 40, 12,6.3));
        panneaus.add(new Panneau("305W-12V Mono", 40, 12,10.27));
        panneaus.add(new Panneau("360W-12V Mono", 40, 12,10.24));


        ArrayAdapter<CharSequence> langAdapter = new ArrayAdapter<CharSequence>(this, R.layout.spinner_text, puissance);
        langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        myspin.setAdapter(langAdapter);

        btn = (Button) findViewById(R.id.btnsanstock);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mavie = myspin.getSelectedItem().toString();
                nomPanneau = mavie;
                for (int i =0; i< panneaus.size(); i++ )
                {
                    if (mavie.matches(panneaus.get(i).getNom()) )
                    {
                        icc = panneaus.get(i).getIcc();
                        tensionPanneau = panneaus.get(i).getTension();
                        puissancePanneau = panneaus.get(i).getPuissance();
                    }
                }

                nomPanneau = Infosansstockage.nomPanneau;
                int nbsresult=0;
                int nbpresult=0;
                int npanneausresultat=0;
                int npanneaupresultat=0;
                int puissancetotale =0;
                double nbs = 0;
                double nbp = 0;
                double nps = 0;
                double nsp = 0;


                nps = Adapter.tensionsysteme /tensionPanneau;
                npanneausresultat= (int) Math.ceil(nps);
                nombrepanneauserie = npanneausresultat;
                nsp = Adapter.pc / (nps * puissancePanneau);
                npanneaupresultat  =  (int) Math.ceil(nsp);
                nombrepanneauparrallele = npanneaupresultat;
                int monpanneau = npanneaupresultat*npanneausresultat;
                if((nombrepanneauserie == 1 && nombrepanneauparrallele ==0) ||(nombrepanneauserie == 0 && nombrepanneauparrallele ==1))
                {
                    nombrepanneau= 1;
                }
                else
                {
                    nombrepanneau =  monpanneau;
                }

                iccs = nombrepanneauparrallele * icc * 1.25;

                Bundle dataBundle = new Bundle();
                dataBundle.putDouble("icc", icc);
                Intent intent = new Intent(getApplicationContext(), Resultat.class);
                intent.putExtras(dataBundle);
                startActivity(intent);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.propos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), Propos.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
