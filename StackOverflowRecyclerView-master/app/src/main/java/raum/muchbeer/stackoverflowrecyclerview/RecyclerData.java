package raum.muchbeer.stackoverflowrecyclerview;

import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

public class RecyclerData {
    //déclaration des attributs de la calsse
    String title;
    Integer position;
    String puissance;
    String nombre;
    String duree;
    Integer puissancetotale;
    Integer energie;

    public Integer getPuissancetotale() {
        return puissancetotale;
    }
    //setter de l'attribut position pour passer des valeur à l'attribut position
    public void setPuissancetotale(Integer puissancetotale) {
        this.puissancetotale = puissancetotale;
    }

    public Integer getEnergie() {
        return energie;
    }
    //setter de l'attribut position pour passer des valeur à l'attribut position
    public void setEnergie(Integer energie) {
        this.position = energie;
    }

    //getter de l'attribut position pour retourner la position du composant
    public Integer getPosition() {
        return position;
    }
    //setter de l'attribut position pour passer des valeur à l'attribut position
    public void setPosition(Integer position) {
        this.position = position;
    }
    //getter de l'attribut title pour retourner le titre du composant
    public String getTitle() { return title; }
    //setter de l'attribut title pour passer des valeur à l'attribut title
    public void setTitle(String title) {
        this.title = title;
    }
    //getter de l'attribut image pour retourner l'image du composant
    public void setCrossImage(ImageView crossImage){
        setCrossImage(crossImage);
    }
    //getter de l'attribut puissance pour retourner la puissance du composant
    public String getPuissance() { return puissance; }
    //setter de l'attribut position pour passer des valeur à l'attribut puissance
    public void setPuissance(String puissance) {this.puissance = puissance; }
    //getter de l'attribut duree pour retourner la duree du composant
    public String getDuree() {
        return duree;
    }
    //setter de l'attribut duree pour passer des valeur à l'attribut duree
    public void setDuree(String duree) {
        this.duree = duree;
    }
    //getter de l'attribut nombre pour retourner le nombre du composant
    public String getNombre() {
        return nombre;
    }
    //setter de l'attribut nombre pour passer des valeur à l'attribut nombre
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
