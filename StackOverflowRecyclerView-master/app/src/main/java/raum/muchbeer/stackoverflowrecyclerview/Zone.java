package raum.muchbeer.stackoverflowrecyclerview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class Zone extends AppCompatActivity {
    Spinner spinner;
    String ville ="";
    public static String maville;
    Button btnNext;
    static{
        maville="";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone);
        spinner = (Spinner) findViewById(R.id.spinner2);
      String[] vie = {"ABOMEY","ABOMEY-CALAVI","ADJARA","ADJA-OUERE","ADJOHOUN","AGBANGNIZOUN",
                "AGUEGUES","AKPRO-MISSERETE","APLAHOUE","ALLADA","ATHIEME","AVRANKOU","BANIKOIRA",
                "BANTE","BASSILA","BEMBEREKE","BOHICON","BOUKOUMBE","BONOU","BOPA","COBLY","COME","COPARGO",
                "COTONOU","COVE","DANGBO","DASSA-ZOUME","DJAKOTOMEY","DJIDA","DJOUGOU","DOGBO",
                "GOGOUNOU","GLAZOUE","GRAND-POPO","HOUEYOGBE","IFANGNI","KALALE","KANDI",
                "KARIMAMA","KEROU","KETOU","KLOUEKANME","KPOMASSE","KOUANDE","LALO",
                "LOKOSSA","MALANVILLE","MATERI","NATITINGOU","NIKKI","N’DALI"
                ,"OUAKE","OUESSE","OUIDAH","OUINHI","PARAKOU","PERERE","PEHUNCO","POBE","PORTO-NOVO",
                "SAKETE","SAVE","SAVALOU", "SEGBANA","SEME-KPODJI","SINENDE","Sô-AVA","TANGUIETA",
                "TCHAOUROU","TOUCOUNTOUNA","TOFFO","TORI-BOSSITO",
                "TOVIKLIN","ZE","ZA-KPOTA","ZAGNANADO","ZOGBODOMEY"};
        ArrayAdapter<CharSequence> langAdapter = new ArrayAdapter<CharSequence>(this, R.layout.spinner_text, vie );
        langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        spinner.setAdapter(langAdapter);
        btnNext = (Button) findViewById(R.id.btnnextzone);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ville = spinner.getSelectedItem().toString();
                maville = ville;
                double irradiation = 0;
                if (ville.matches("BANIKOIRA") || ville.matches("GOGOUNOU")||ville.matches("KANDI")||ville.matches("KARIMAMA") ||ville.matches("MALANVILLE") ||ville.matches("SEGBANA") ||ville.matches("KEROU")||ville.matches("KOUANDE")||ville.matches("PEHUNCO")||ville.matches("TANGUIETA")||ville.matches("TOUCOUNTOUNA") ||ville.matches("BEMBEREKE")||ville.matches("KALALE")||ville.matches("N’DALI")||ville.matches("NIKKI")||ville.matches("PARAKOU")||ville.matches("SINENDE")||ville.matches("COPARGO")||ville.matches("DJOUGOU")) {
                    irradiation = 5.75342;
                }
                else if (ville.matches("ALLADA")||ville.matches("KPOMASSE")||ville.matches("TOFFO")||ville.matches("TORI-BOSSITO")||ville.matches("ZE")||ville.matches("DJAKOTOMEY")||ville.matches("DOGBO")||ville.matches("KLOUEKANME")||ville.matches("LALO")||ville.matches("TOVIKLIN")||ville.matches("ATHIEME")||ville.matches("BOPA")||ville.matches("DJAKOTOMEY")||ville.matches("DOGBO")||ville.matches("KLOUEKANME")||ville.matches("LALO")||ville.matches("TOVIKLIN")||ville.matches("ATHIEME")||ville.matches("BOPA")) {
                    irradiation = 4.9315;
                }
                else if (ville.matches("BOUKOUMBE")||ville.matches("COBLY")||ville.matches("MATERI")||ville.matches("NATITINGOU")||ville.matches("BASSILA")||ville.matches("OUAKE")||ville.matches("TCHAOUROU")||ville.matches("PERERE")||ville.matches("BANTE")) {
                    irradiation = 5.47945;
                }
                else if (ville.matches("APLAHOUE") || ville.matches("BOHICON")) {
                    irradiation = 5.2055;
                }
                else if (ville.matches("COTONOU")||ville.matches("COME")||ville.matches("GRAND-POPO")||ville.matches("AGUEGUES")||ville.matches("SEME-KPODJI")|| ville.matches("ABOMEY")||ville.matches("AGBANGNIZOUN") ||ville.matches("DJIDA") ||ville.matches("OUINHI")||ville.matches("ZA-KPOTA")||ville.matches("DASSA-ZOUME")||ville.matches("GLAZOUE")||ville.matches("OUESSE")||ville.matches("SAVALOU") ||ville.matches("ABOMEY-CALAVI")||ville.matches("OUIDAH")||ville.matches("Sô-AVA")  ) {
                    irradiation = 5.20547;
                }
                Bundle dataBundle = new Bundle();
                dataBundle.putDouble("irradiation", irradiation);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(dataBundle);
                startActivity(intent);

               /* Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                intent.putExtra("monirradiation", irradiation);
                startActivity(intent);*/
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.propos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), Propos.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
