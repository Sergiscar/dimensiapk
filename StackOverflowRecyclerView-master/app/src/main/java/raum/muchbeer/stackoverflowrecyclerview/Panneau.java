package raum.muchbeer.stackoverflowrecyclerview;

public class Panneau {

    private String nom;
    private int puissance;
    private int tension;
    private double icc;

    public Panneau(String nom, int puissance, int tension, double icc) {
        this.nom = nom;
        this.puissance = puissance;
        this.tension = tension;
        this.icc = icc;
    }

    public String getNom() {
        return nom;
    }

    public int getPuissance() {
        return puissance;
    }

    public int getTension() {
        return tension;
    }

    public double getIcc() {
        return icc;
    }
}
