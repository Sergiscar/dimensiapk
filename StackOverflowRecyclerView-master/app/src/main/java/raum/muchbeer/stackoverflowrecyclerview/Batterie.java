package raum.muchbeer.stackoverflowrecyclerview;

public class Batterie  {
    private String nom;
    private double intensite ;
    private double tension;


    public Batterie(String nom, double tension, double intensite) {
        this.nom = nom;
        this.tension = tension;
        this.intensite = intensite;

    }

    public String getNom() {
        return nom;
    }

    public double getIntensite() {
        return intensite;
    }

    public double getTension() {
        return tension;

    }

}
